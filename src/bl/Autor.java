package bl;

import java.time.*;
/*
/ @autor: Percy Jimenez M
/ @version:1, 04/25/2021
*/

public class Autor extends Persona {

    private String lugarNacimiento;
    private String nacionalidad;
    private LocalDate fechaNacimiento;
    private int edad;

    public Autor() {
        super();
        this.lugarNacimiento = "";
        this.nacionalidad = "";
        this.fechaNacimiento = null;
        this.edad = 0;
    }

    public Autor(String paisNacimiento, String identificacion, String nombreCompleto, String lugarNacimiento, String nacionalidad, LocalDate fechaNacimiento, int edad) {
        super(paisNacimiento, identificacion, nombreCompleto);
        this.lugarNacimiento = lugarNacimiento;
        this.nacionalidad = nacionalidad;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
    }

    public Autor(String paisNacimiento, String nombreCompleto, String lugarNacimiento, String nacionalidad, LocalDate fechaNacimiento, int edad) {
        super(paisNacimiento, nombreCompleto);
        this.lugarNacimiento = lugarNacimiento;
        this.nacionalidad = nacionalidad;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String toString() {
        return "Autor{" + "lugarNacimiento=" + lugarNacimiento + ", nacionalidad="
                + nacionalidad + ", fechaNacimiento=" + fechaNacimiento + ", edad=" + edad + '}';
    }

}//fin
