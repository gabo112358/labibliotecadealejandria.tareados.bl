package bl;

import java.time.*;

public class Libro {

    private LocalDate fechaPublicacion;
    private String nombreObra;
    private LocalDate fechaIngreso;
    private int cantidadPaginas;
    private int ISBN;
    private Autor autor;
    private Categorias categoriaLibro;

    public Libro() {
        this.fechaPublicacion = null;
        this.nombreObra = "";
        this.fechaIngreso = null;
        this.cantidadPaginas = 0;
        this.ISBN = 0;
    }

    public Libro(LocalDate fechaPublicacion, String nombreObra, LocalDate fechaIngreso, int cantidadPaginas, int ISBN, Autor autor, Categorias categoriaLibro) {
        this.fechaPublicacion = fechaPublicacion;
        this.nombreObra = nombreObra;
        this.fechaIngreso = fechaIngreso;
        this.cantidadPaginas = cantidadPaginas;
        this.ISBN = ISBN;
        this.autor = autor;
        this.categoriaLibro = categoriaLibro;
    }

    public Libro(String nombreObre) {
        this.nombreObra = nombreObra;
    }

    public LocalDate getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(LocalDate fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getNombreObra() {
        return nombreObra;
    }

    public void setNombreObra(String nombreObra) {
        this.nombreObra = nombreObra;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getCantidadPaginas() {
        return cantidadPaginas;
    }

    public void setCantidadPaginas(int cantidadPaginas) {
        this.cantidadPaginas = cantidadPaginas;
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Categorias getCategoriaLibro() {
        return categoriaLibro;
    }

    public void setCategoriaLibro(Categorias categoriaLibro) {
        this.categoriaLibro = categoriaLibro;
    }

    public String toString() {
        return "Libro{" + "fechaPublicacion=" + fechaPublicacion
                + ", nombreObra=" + nombreObra + ", fechaIngreso="
                + fechaIngreso + ", cantidadPaginas=" + cantidadPaginas
                + ", ISBN=" + ISBN + ", autor=" + autor + ", categoriaLibro="
                + categoriaLibro + '}';
    }

}//fin
