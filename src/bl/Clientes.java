package bl;

public class Clientes extends Persona {

    private Libro libroCliente;

    public Clientes() {
        this.libroCliente = null;
    }

    public Clientes(String paisNacimiento, String identificacion, String nombreCompleto, Libro libroCliente) {
        super(paisNacimiento, identificacion, nombreCompleto);
        this.libroCliente = libroCliente;
    }

    public Libro getLibroCliente() {
        return libroCliente;
    }

    public void setLibroCliente(Libro libroCliente) {
        this.libroCliente = libroCliente;
    }
   
     public String getNombreLibro() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String toString() {
        return "Clientes{" + "libroCliente=" + libroCliente + '}';
    }

   

}//fin
