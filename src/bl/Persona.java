package bl;


public class Persona {
    private String paisNacimiento;
    private String identificacion;
    private String nombreCompleto; 

    public Persona() {
        this.paisNacimiento = "";
        this.identificacion = "";
        this.nombreCompleto = "";
    }

    public Persona(String paisNacimiento, String identificacion, String nombreCompleto) {
        this.paisNacimiento = paisNacimiento;
        this.identificacion = identificacion;
        this.nombreCompleto = nombreCompleto;
    }

    public Persona(String paisNacimiento, String nombreCompleto) {
        this.paisNacimiento = paisNacimiento;
        this.nombreCompleto = nombreCompleto;
    }

    public String getPaisNacimiento() {
        return paisNacimiento;
    }

    public void setPaisNacimiento(String paisNacimiento) {
        this.paisNacimiento = paisNacimiento;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }


    public String toString() {
        return "Persona{" + "paisNacimiento=" + paisNacimiento 
                + ", identificacion=" + identificacion + ", nombreCompleto=" 
                + nombreCompleto + '}';
    }
    
    
}//fin
